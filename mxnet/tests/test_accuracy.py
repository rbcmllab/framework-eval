import pytest

from run import accuracy

import mxnet.ndarray as nd

def test_accuracy():
    output = nd.array([1, 0, 1, 1])
    target = nd.array([0, 0, 1, 0])

    batch_size = output.shape[0]

    res = accuracy(output, target)
    assert res.__eq__( 2 * (100.0 / batch_size)).asscalar()
