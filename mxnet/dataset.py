import pickle 

from mxnet import gluon
import mxnet.ndarray as nd


class IMDBDataset(gluon.data.Dataset):
    """

    """

    def __init__(self, data_path: str, sequence_length=500):
        with open(data_path, 'rb') as fin:
            self.inputs, self.labels, self.vocab_size = pickle.load(fin) 
        self.seq_len = sequence_length
        
    def __len__(self):
        return len(self.inputs)
    
    def __getitem__(self, index):
        seq = nd.array(self.inputs[index])
        label = nd.array([self.labels[index]])
        return seq, label
