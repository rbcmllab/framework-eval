import torch.nn as nn
import torch.nn.functional as F
import torch


class IMDBNet(nn.Module):
    """

    """

    def __init__(self, embedding_dim, vocab_size, hidden_dim, nClasses, sparse_embeddings=False):
        super(IMDBNet, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim, sparse=sparse_embeddings)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=1, batch_first=True)
        self.linear = nn.Linear(hidden_dim, nClasses)
        self._init_weights()

    def _init_weights(self):
        torch.nn.init.xavier_normal(self.embedding.weight)
        torch.nn.init.xavier_normal(self.linear.weight)
        for name, param in self.lstm.named_parameters():
            if 'bias' in name:
                nn.init.constant(param, 0.0)
            else:
                nn.init.xavier_normal(param)

        # I had to add the call to flatten_parameters to avoid the warning about parameters existing in non-
        # contiguous memory. Without this, I eventually exceed memory cap on our machines.
        self.lstm.flatten_parameters()

    def forward(self, x):
        embeds = self.embedding(x)

        lstm_out, self.hidden = self.lstm(embeds)

        # Average along sequence dimension to get mean hidden state for sentence
        mean_lstm = torch.mean(lstm_out, 1)

        # We take the last hidden state in the dimension of the sequence length since we're doing sentence level
        # classification
        logits = self.linear(mean_lstm)

        return logits
