#!/usr/bin/env bash

python run.py ../data \
            $1 \
            --num-epochs 200 \
            --batch-size 512 \
            --num-gpus 1 \
            --num-workers 1\
            --adjust-lr \

