#!/usr/bin/env python3

"""
Downlaods data from the IMDB Reviews dataset using Keras, with configurable values
for vocabulary size and sequence length. 
"""

import os
import argparse
import pickle

from keras.datasets import imdb
from keras.preprocessing import sequence


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--vocab-size', type=int, required=True)
    parser.add_argument('--sequence-length', type=int, required=True)

    args = parser.parse_args()

    (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=args.vocab_size)
    X_train = sequence.pad_sequences(X_train, maxlen=args.sequence_length)
    X_test = sequence.pad_sequences(X_test, maxlen=args.sequence_length)

    with open("train_data.pkl", 'wb') as fout:
        pickle.dump((X_train, y_train), fout)

    with open("test_data.pkl", 'wb') as fout:
        pickle.dump((X_test, y_test), fout)


if __name__ == "__main__":
    main()
